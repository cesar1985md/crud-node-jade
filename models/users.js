var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost:27017/testnode', { useNewUrlParser: true });
/*
String
Number
Date
Buffer
Boolean
Mixed
Objectid
Array
*/

var sexos = ['M', 'F'];

var email_expr = [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Coloca un email válido'];

var user_schema = new Schema({
    name: String,
    last_name: String,
    username: {
        type: String,
        required: true,
        maxlength: [50, 'No puede tener mas de 50 carácteres']
    },
    password: {
        type: String,
        minlength: [5, 'El password es muy corto'],
        validate: {
            validator: function(p) {
                return this.password_confirmation == p;
            },
            message: 'Las contraseñas no son iguales'
        }
    },
    age: {
        type: Number,
        min: [5, 'La edad no puede ser menos que 5 años'],
        max: [99, 'La edad no puede ser mayor de 99 años']
    },
    email: {
        type: String,
        required: 'El correo es obligatorio',
        match: email_expr
    },
    date_of_birdth: Date,
    sex: {
        type: String,
        enum: { values: sexos, message: 'Opción no válida' }
    }
})

// VIRTUALS ++++++++++++++++++++++++++++++++++++++++++++++++++++
user_schema.virtual('password_confirmation').get(function () {
    return this.p_c;
}).set(function (password) {
    this.p_c = password;
})

user_schema.virtual('full_name').get(function () {
    return this.name + this.last_name;
}).set(function (full_name) {
    var words = full_name.split(' ');
    this.name = words[0];
    this.last_name = words[1];
})
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


// ESTO EQUIVALE A UNA TABLA, SI NO EXISTE CREA LA COLECCION EN PLURAL "Users"
// TODA LA COMUNICACION CON MONGO SE HACE MEDIANTE MODELOS
var User = mongoose.model('User', user_schema);

module.exports.User = User;

