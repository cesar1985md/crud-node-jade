var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost:27017/testnode', { useNewUrlParser: true });

var img_schema = new Schema ({
    title: {
        type: String
    },
    creator: {
        type: Schema.Types.ObjectId, ref: 'User'
    },
    extension: {
        type: String
    }
});

var Imagen = mongoose.model('Imagen', img_schema);

module.exports = Imagen;