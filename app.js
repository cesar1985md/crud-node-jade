var express = require('express');
var bodyParser = require('body-parser');
var User = require('./models/users').User;
var app = express();
// var session = require('express-session');
var cookieSession = require('cookie-session');
// CARGAMOS EL SISTEMA DE RUTAS
var router_app = require('./routes_app');
var session_middleware = require('./middlewares/session');

var method_override = require('method-override');

var formidable = require('express-formidable');


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// MONGO DB - TODO ESTO SE HA PASADO AL ARCHIVO users.js
// var mongoose = require('mongoose');
// http://localhost:27017/testnode
// mongoose.connect('mongodb://localhost:27017/testnode', { useNewUrlParser: true });
// var Schema = mongoose.Schema;

// Creamos una tabla/documento, los registros de las tablas normales en mongo son objetos
// COLECCIONES -> TABLAS
// DOCUMENTOS -> FILAS
// var userSchemaJSON = {
//     email:String,
//     pass: String
// };
// var user_schema = new Schema(userSchemaJSON);
// Creamos un modelo
// var User = mongoose.model('User', user_schema);
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// PARA PODER USAR OTROS METODOS EN LOS FORMULARIOS APARTE DE GET POST
app.use(method_override('_method'));
// PARA PODER USAR JADE
app.set('view engine', 'jade');
// MIDDLEWARE
// SERVIR ARCHIVOS ESTÁTICOS
app.use('/public', express.static('public'));
// BODY PARSER MIDDLEWARE
// Aplicaciones que tengan el formato application/json
app.use(bodyParser.json());
// Si usamos el parametro false, no podremos parsear arrays.
app.use(bodyParser.urlencoded({ extended: true }));

// USO DE SESIONES
// app.use(session({
//     secret: '123asd123asd',
//     resave: false,
//     saveUninitialized: false
// }));


// USO DE COOKIES
app.use(cookieSession({
    name: 'session',
    keys: ['llave-1', 'llave-2'] 
}));

// PETICIONES HTTP - ARQUITECTURA REST
// GET / POST / PUT / PATCH / OPTIONS / HEADERS / DELETE
app.get('/', function (req, res) {
    res.render('index');
});

app.get('/signup', function (req, res) {
    // OBTENER TODOS LOS USUARIOS DE LA BASE DE DATOS
    // User.find(function (err, doc) {
    //     if (err == null) {
    //         console.log(doc);
    //     }
    // })
    res.render('signup');
});

app.get('/login', function (req, res) {
    res.render('login');
});

// USAMOS PARA VER SI ESTA REGISTRADO UN USUARIO
app.post('/sessions', function (req, res) {
    // User.findById('')
    // User.find() -> Devuelve varios registros
    User.findOne({ email: req.body.email, password: req.body.password }, function (err, user) {
        req.session.user_id = user._id;
        res.redirect('/app');
    });
})

// USAMOS PARA GUARDAR UN NUEVO REGISTRO DE USUARIO
app.post('/users', function (req, res) {
    //GUARDAMOS EL USUARIO EN MONGODB
    var user = new User({
        email: req.body.email,
        password: req.body.password,
        password_confirmation: req.body.password_confirmation,
        username: req.body.username
    });
    //EN ERR SE OBTIENEN LOS ERRORES QUE HEMOS DECLARADO EN EL SCHEMA 'users.js'
    // user.save(function (err, saveduser, numelements) {
    //     if (err) {
    //         console.log(err);
    //     } else {
    //         res.send('Hemos guardado los datos');
    //     }
    // })

    // PROMESAS / PROMISES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    // METODO SAVE
    user.save().then(function (user) {
        res.send('Usuario Guardado');
    }, function (err) {
        if (err) {
            res.send('No pudimos guardar la información');
        }
    })
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
});

// CARGAMOS LOS DATOS DEL USUARIO SI ESTA REGISTRADO
app.use('/app', session_middleware);
// DEFINIMOS QUE LA RAIZ PRINCIPAL ES APP
app.use('/app', router_app);

app.listen(4200);