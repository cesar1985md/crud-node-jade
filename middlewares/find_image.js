var Imagen = require('../models/imagenes');
var owner_check = require('./image_permission');

module.exports = function (req, res, next) {
    Imagen.findById(req.params.id)
        .populate('creator')
        .exec(function (err, imagen) {
            if (imagen != null && owner_check(imagen, req, res)) {
                res.locals.imagen = imagen;
                next();
            } else {
                res.redirect('/app');
            }
        })
}

// module.exports = function (req, res, next) {
//     Imagen.findById(req.params.id, function (err, imagen) {
//         // console.log('REQ: ', req);
//         // console.log('Encontrado', imagen.title);
//         if (imagen != null) {
//             res.locals.imagen = imagen;
//             console.log(res.locals);
//             next();
//         } else {
//             res.redirect('/app');
//         }
//     })
// }