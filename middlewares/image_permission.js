var Imagen = require('../models/imagenes');

module.exports = function (image, req, res) {
    // True -> Tienes permisos
    // False -> Si no tienes permisos
    if (res.method === 'GET' && res.path.indexOf('edit') < 0) {
        return true;
    }

    if (image.creator._id.toString() == res.locals.user._id) {
        return true;
    }
    return false;
}