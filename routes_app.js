var express = require('express');
var Imagen = require('./models/imagenes');
var router = express.Router();
var image_finder_middleware = require('./middlewares/find_image');

var formidable = require('formidable');

var fs = require('fs');

router.get('/', function (req, res) {
    res.render('app/home');
})

/* ARQUITECTURA REST - CRUD */
/*  CREATE
    READ
    UPDATE
    DELETE */

router.get('/imagenes/new', function (req, res) {
    res.render('app/imagenes/new');
});

router.all('/imagenes/:id*', image_finder_middleware);

router.get('/imagenes/:id/edit', function (req, res) {
    res.render('app/imagenes/edit')
});

router.route('/imagenes/:id')
    .get(function (req, res) {
        // Imagen.findById(req.params.id, function (err, imagen) {
        res.render('app/imagenes/show');
        // })
    })
    .put(function (req, res) {
        // Imagen.findById(req.params.id, function (err, imagen) {
        res.locals.imagen.title = req.body.titulo;
        res.locals.imagen.save(function (err) {
            if (!err) {
                res.render('app/imagenes/show');
            } else {
                res.render('app/imagenes/' + req.params.id + 'edit');
            }
        })
        res.render('app/imagenes/show');
        // })
    })
    .delete(function (req, res) {
        Imagen.findByIdAndRemove({ _id: req.params.id }, function (err) {
            if (!err) {
                res.redirect('/app/imagenes');
            } else {
                res.redirect('/app/imagenes' + req.params.id)
            }
        })
    });

router.route('/imagenes')
    .get(function (req, res) {
        Imagen.find({ creator: res.locals.user._id }, function (err, imagenes) {
            if (err) {
                res.redirect('/app');
                return;
            }
            res.render('app/imagenes/index', { imagenes: imagenes });
        })
    })
    .post(function (req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            // Sacamos la extension de la imagen
            var extension = files.archivo.name.split('.').pop();
            // `archivo` is the name of the <input> field of type `file`
            var data = {
                title: fields.titulo,
                creator: res.locals.user._id,
                extension: extension
            }
            var imagen = new Imagen(data);
            
            var oldpath = files.archivo.path;
            var newpath = 'D:/Proyectos/node/proyecto/public/imagenes/' + imagen._id + extension;

            imagen.save(function (err, imagen) {
                if (!err) {
                    fs.move(oldpath, newpath, function (err) {
                        if (err) throw err;
                        res.redirect('/app/imagenes/' + imagen._id)
                    });
                } else {
                    res.render(err);
                }
            })
        });
    });
module.exports = router;